/**
 * Emilian Wilczek
 * UHI 16013977
 * Pontoon game
 */

package emilianwilczek;

import java.util.Scanner;

/**
 * Class with various types of validated user inputs.
 */
public class Input
{
    private static Scanner keyboard = new Scanner(System.in);

    /**
     * Validates the given integer. Loops and asks for another input if an integer isn't given.
     * @return Returns the given integer.
     */
    public static int Integer()
    {
        while(true)
        {
            String input = keyboard.next();
            try
            {
                return Integer.parseInt(input);
            }
            catch (NumberFormatException nfe)
            {
                System.out.println("Input an integer please.");
            }
        }
    }

    /**
     * Validates the given integer. Loops and asks for another input if an integer isn't given or if it's not within the specified range.
     * @param min Minimum accepted integer value.
     * @param max Maximum accepted integer value.
     * @return Returns given integer if within the accepted range.
     */
    public static int Integer(int min, int max)
    {
        while(true)
        {
            String input = keyboard.next();
            try
            {
                int parsedInput = Integer.parseInt(input);
                if (parsedInput >= min && parsedInput <= max)
                {
                    return Integer.parseInt(input);
                }
                else
                {
                    System.out.println("Input an integer between " + min + " and " + max + " please.");
                }
            }
            catch (NumberFormatException nfe)
            {
                System.out.println("Input an integer between " + min + " and " + max + " please.");
            }
        }
    }

    /**
     * Looks for multiple text versions of "yes" and "no" and returns a boolean value.
     * @return Returns boolean based on given answer.
     */
    public static boolean YesOrNo()
    {
        while(true)
        {
            String input = keyboard.next().toLowerCase();
            switch(input)
            {
                case "y", "yes", "ye", "yeah", "yep": return true;
                case "n", "no", "nope": return false;
                default: System.out.println("Input Y/N or Yes/No please.");
            }
        }
    }

    /**
     * Asks for a String input and returns it.
     * @return Returns given String.
     */
    public static String String()
    {
        while(true)
        {
            String input = keyboard.next();
            return input;
        }
    }
}
