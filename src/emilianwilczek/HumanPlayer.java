/**
 * Emilian Wilczek
 * UHI 16013977
 * Pontoon game
 */

package emilianwilczek;

import java.util.ArrayList;

/**
 * Human player class that displays information and asks for inputs.
 */
public class HumanPlayer extends Player
{
    private boolean isStanding = false;

    /**
     * Draws a card from a given deck if confirmed with a text input. Stands otherwise.
     * @param cards Any given deck of cards.
     */
    public void drawCardInput(ArrayList<Integer> cards)
    {
        System.out.println("Do you want to draw a card?");
        if (Input.YesOrNo())
        {
            drawCard(cards);
            System.out.println("You have drawn:");
            System.out.println(getHand());
            setStanding(false);
        }
        else
        {
            System.out.println("You have not drawn.");
            setStanding(true);
        }
    }

    public void setStanding(boolean isStanding)
    {
        this.isStanding = isStanding;
    }

    public boolean isStanding()
    {
        return isStanding;
    }

    public boolean hasWon()
    {
        if (getHandTotal() == 21)
        {
            System.out.println("YOU WIN!");
            return true;
        }
        else return false;
    }

    public boolean hasBusted()
    {
        if (getHandTotal() > 21)
        {
            System.out.println("BUSTED");
            return true;
        }
        else return false;
    }
}
