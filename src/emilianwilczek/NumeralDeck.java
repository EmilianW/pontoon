/**
 * Emilian Wilczek
 * UHI 16013977
 * Pontoon game
 */

package emilianwilczek;

import java.util.ArrayList;

/**
 * A deck of numeral cards (i.e. non-face cards). 4 suits of 10 cards with values from 1 to 10.
 */
public class NumeralDeck
{
    ArrayList<Integer> cards = new ArrayList<>();

    private final int NoOfSuits = 4;
    private final int NoOfNumerals = 10;

    public NumeralDeck()
    {
        for(int j = 0; j < NoOfSuits; j++)
        {
            for (int i = 0; i < NoOfNumerals; i++)
            {
                cards.add(i + 1);
            }
        }
    }
}
