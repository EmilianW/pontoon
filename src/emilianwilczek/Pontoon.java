/**
 * Emilian Wilczek
 * UHI 16013977
 * Pontoon game
 */

package emilianwilczek;

/**
 * Game logic.
 */
public class Pontoon
{
    /**
     * Gameplay loop.
     */
    public void play()
    {
        while(true)
        {
            NumeralDeck deck = new NumeralDeck();
            HumanPlayer player = new HumanPlayer();
            ComputerPlayer house = new ComputerPlayer();

            while (!player.hasWon() && !player.hasBusted() && !player.isStanding())
            {
                player.drawCardInput(deck.cards);
                house.drawCard(deck.cards);
                System.out.println(house.getHand());
            }
            if (house.getHandTotal() > player.getHandTotal()) System.out.println("HOUSE WINS!");
            else System.out.println("YOU WIN!");
        }
    }
    }
