/**
 * Emilian Wilczek
 * UHI 16013977
 * Pontoon game
 */

package emilianwilczek;

public class Launcher
{

    public static void main(String[] args) {
        Pontoon pontoon = new Pontoon();
        pontoon.play();
    }
}
