/**
 * Emilian Wilczek
 * UHI 16013977
 * Pontoon game
 */

package emilianwilczek;

import java.util.ArrayList;

abstract class Player
{
    private String name;

    private ArrayList<Integer> hand = new ArrayList<>();

    public Player()
    {
        this.name = "Player";
    }

    public String getName()
    {
        return name;
    }

    public ArrayList<Integer> getHand()
    {
        return hand;
    }

    public void setName(String newName)
    {
        this.name = newName;
    }

    /**
     * Draws a card from a deck and puts in into the hand.
     * @param cards
     */
    public void drawCard(ArrayList<Integer> cards)
    {
        int availableCards = cards.size();
        int drawnCard = (int)(Math.random() * availableCards);

        hand.add(cards.get(drawnCard));

        cards.remove(drawnCard);
    }

    /**
     * Prompts for a new name, changes the value and prints out the new name.
     */
    public void changeName()
    {
        System.out.println("Input name: ");

        this.setName(Input.String());

        System.out.println("Your name is now " + getName());
    }

    /**
     * Gets total of the cards in hand.
     * @return Total of the cards in hand.
     */
    public int getHandTotal()
    {
        int total = 0;
        for (int card : getHand())
        {
            total += card;
        }
        return total;
    }




}
